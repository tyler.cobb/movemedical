package com.moveMedical.assignment.facade;

import com.moveMedical.assignment.constants.NodeConstants;
import com.moveMedical.assignment.dao.NodeDao;
import com.moveMedical.assignment.objects.BaseResponseDto;
import com.moveMedical.assignment.objects.Node;
import com.moveMedical.assignment.objects.Record;
import com.moveMedical.assignment.objects.TreeResponseDto;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Facade class for the business logic of the
 * node operations
 */
@Component
public class NodeFacade {

    private static final Logger logger= LoggerFactory.getLogger(NodeFacade.class);
    private NodeDao nodeDao;

    @Autowired
    public NodeFacade(NodeDao nodeDao){
        this.nodeDao = nodeDao;
    }

    /**
     * InsertRecord facade that hits the dao and checks the response
     *
     * @param jsonRequest the record that is being inserted
     * @return BaseDtoResponse with the results of the insert
     */
    public BaseResponseDto insertRecord(Record jsonRequest){
        logger.debug("Enter - insertRecordfacade");
        List<String> duplicates = new ArrayList<>();
        try {
            duplicates = nodeDao.insert(jsonRequest);
            return checkInsertDuplicates(duplicates);
        }catch(Exception ex){
            logger.error("Error on inserting Record with top level name: " + jsonRequest.getName(), ex);
            return new BaseResponseDto(NodeConstants.ERROR_ON_INSERT_FAILURE,
                    NodeConstants.ERROR_TYPE_INSERT_FAILURE,
                    NodeConstants.ERROR_DESCRIPTION_INTER_RECORD);
        }
    }

    /**
     * Used to check size of the list returned that represents
     * duplicate names attempted to insert into the database
     *
     * @param duplicates list of duplicate names
     * @return response based on if there are duplicates or not
     */
    private BaseResponseDto checkInsertDuplicates(List<String> duplicates) {
        if(duplicates.size() == 0){
            logger.trace("No duplicates on insert");
            return new BaseResponseDto(NodeConstants.RESULT_SUCCESS,
                    NodeConstants.ERROR_TYPE_SUCCESS,
                    NodeConstants.ERROR_DESCRIPTION_SUCESS);
        }else {
            logger.error("Duplicates on insert. names of duplicates: " + duplicates.toString());
            return new BaseResponseDto(NodeConstants.ERROR_ON_INSERT_EXIST,
                    NodeConstants.ERROR_TYPE_INSERT_NODE_EXIST,
                    duplicates.size() + NodeConstants.ERROR_DESCRIPTION_INTER_DUPLICATES
                            + duplicates.toString());
        }
    }

    /**
     * Method that calls the dao to get a tree structure
     *
     * @param nodeName name of the parent node to search
     * @return response dto based on the tree returned
     */
    public TreeResponseDto getTreeStructure(String nodeName) {
        logger.debug("Enter - getTreeStructureFacade");
        String response = "";
        try {
            List<Node> node = nodeDao.getTreeStructure(nodeName);
            if(node == null){
                return validateTree(response);
            }
            Node nodeReturned = populateRecordTree(node);
            String temp = nodeReturned.toString();
            JSONObject jsonObject = new JSONObject(temp);
            return validateTree(jsonObject.toString(4));
        }catch (Exception ex){
            logger.error("Error getting tree structure", ex);
            return new TreeResponseDto("",
                    NodeConstants.ERROR_ON_GET_TREE,
                    NodeConstants.ERROR_TYPE_GET_TREE_FAILURE,
                    NodeConstants.ERROR_DESCRIPTION_GET_TREE_FAILURE);
        }
    }

    /**
     * Checks the tree response and returns the DTO based
     * on if the tree is empty or not
     *
     * @param response response from the database
     * @return ResponseDTO
     */
    protected TreeResponseDto validateTree(String response){
        if(response.isEmpty()){
            logger.error("Empty tree respone. No node found");
            return new TreeResponseDto("",
                    NodeConstants.ERROR_GET_TREE_EMPTY,
                    NodeConstants.ERROR_TYPE_GET_TREE_EMPTY,
                    NodeConstants.ERROR_DESCRIPTION_GET_TREE_EMPTY);
        }else{
            return new TreeResponseDto(response,
                    NodeConstants.RESULT_SUCCESS,
                    NodeConstants.ERROR_TYPE_SUCCESS,
                    NodeConstants.ERROR_DESCRIPTION_SUCESS);
        }
    }

    /**
     * Takes the list of nodes from the database and puts them
     * in a node hierarchy
     *
     * @param nodes from the database
     * @return root node
     */
    protected Node populateRecordTree(List<Node> nodes){
        Map<Integer, Node> recordMap = new HashMap<>();
        for(Node currentNode: nodes){
            recordMap.put(currentNode.getId(), currentNode);
        }
        for(Node current: nodes){
            Integer parentId = current.getParentId();
            if(parentId != null){
                Node parent = recordMap.get(parentId);
                if(parent != null){
                    current.setParentNode(parent);
                    parent.addChild(current);
                    recordMap.put(parentId, parent);
                    recordMap.put(current.getId(), current);
                }
            }
        }
        Node root = null;
        for (Node node : recordMap.values()) {
            if(node.getParentId() == null) {
                root = node;
                break;
            }
        }
        return root;
    }

    /**
     * delete tree method that removes a tree and it's
     * children based on name.
     *
     * @param nodeName node name to delete
     * @return resposne on the success of the delete
     */
    public BaseResponseDto deleteTree(String nodeName) {
        logger.debug("Enter - deleteTreeFacade");
        try {
            int deleteResult = nodeDao.deleteNodes(nodeName);
            return deleteTreeResponse(deleteResult, nodeName);
        }catch (Exception ex){
            logger.error("Error deleting node: " + nodeName);
            return new BaseResponseDto(
                    NodeConstants.ERROR_DELETE_NODE,
                    NodeConstants.ERROR_TYPE_DELETE_FAILURE,
                    NodeConstants.ERROR_DESCRIPTION_DELETE_FAILURE + nodeName
            );
        }
    }

    /**
     * Method that checks the size of the result returned from the database
     * to make sure a node was found
     *
     * @param deleteResult result int from the dao
     * @param nodeName nodeName of the node
     * @return base response for the result
     */
    private BaseResponseDto deleteTreeResponse(int deleteResult,  String nodeName) {
        if(deleteResult > 0){
            return new BaseResponseDto(
                    NodeConstants.RESULT_SUCCESS,
                    NodeConstants.ERROR_TYPE_SUCCESS,
                    NodeConstants.ERROR_DESCRIPTION_SUCESS
            );
        }else{
            logger.error("No node found to delete. NodeName = " + nodeName);
            return new BaseResponseDto(
                    NodeConstants.ERROR_DELETE_NODE_NO_NODE,
                    NodeConstants.ERROR_TYPE_DELETE_NO_NODE,
                    NodeConstants.ERROR_DESCRIPTION_DELETE_NO_NODE + nodeName
            );
        }
    }
}
