package com.moveMedical.assignment.controller;

import com.moveMedical.assignment.dao.NodeDao;
import com.moveMedical.assignment.facade.NodeFacade;
import com.moveMedical.assignment.objects.BaseResponseDto;
import com.moveMedical.assignment.objects.Record;
import io.swagger.annotations.ApiParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * The Rest Controller that defines the endpoints the user is
 * able to hit.
 */
@RestController
@RequestMapping("/")
public class NodeController {

    private NodeFacade nodeFacade;
    private static final Logger logger= LoggerFactory.getLogger(NodeController.class);

    @Autowired
    public NodeController(NodeDao nodeDao, NodeFacade nodeFacade){
        this.nodeFacade = nodeFacade;
    }

    /**
     * Controller endpoint for inserting a root node or a tree into the database.
     *
     * @param jsonInsert - the record that is going to be inserted
     * @return the BaseResponseDTO with success or error descriptions
     */
    @PostMapping("/insertRecord")
    public BaseResponseDto insertRecord(@ApiParam("Forces this endpoint to throw a generic error.")
                                            @RequestBody Record jsonInsert){
        logger.debug("Enter - insertRecordController");
        return nodeFacade.insertRecord(jsonInsert);
    }

    /**
     * Controller endpoint that gets the tree for a given node name.
     *
     * @param nodeName to search for.
     * @return a String with the response details and the tree formatted in json.
     *        **NOTE** using toString() to the whole response to get the printed out
     *        formatted json
     */
    @GetMapping("/getTree")
    public String getTreeStructure(@ApiParam("Forces this endpoint to throw a generic error.")
                                       @RequestParam String nodeName) {
        logger.debug("Enter - getTreeStructureController");
        return nodeFacade.getTreeStructure(nodeName).toString();
    }

    /**
     * Controller endpoint to delete a node and it's children
     *
     * @param nodeName - name of the node you want to delete
     * @return reponse of the deleted node
     */
    @DeleteMapping("/deleteTree")
    public BaseResponseDto deleteTreeStructure(@ApiParam("Forces this endpoint to throw a generic error.")
                                          @RequestParam String nodeName){
        logger.debug("Enter - deleteTreeController");
        return nodeFacade.deleteTree(nodeName);
    }

}
