package com.moveMedical.assignment.objects;

import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class NodeRowMapper implements RowMapper<Node> {

    @Override
    public Node mapRow(ResultSet rs, int rowNum) throws SQLException {
        Node node = new Node();
        node.setId(rs.getInt("id"));
        node.setName(rs.getString("name"));
        node.setParentId(rs.getInt("parent_id"));
        return node;
    }
}
