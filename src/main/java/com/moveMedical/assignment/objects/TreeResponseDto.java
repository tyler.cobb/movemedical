package com.moveMedical.assignment.objects;

public class TreeResponseDto extends BaseResponseDto{

    private String treeJson;

    public TreeResponseDto(String treeJson, int resultCode, String errorType, String returnDescription) {
        super(resultCode, errorType, returnDescription);
        this.treeJson = treeJson;
    }

    public TreeResponseDto(){

    }
    public TreeResponseDto(String treeJson) {
        this.treeJson = treeJson;
    }

    public String getTreeJson() {
        return treeJson;
    }

    public void setTreeJson(String treeJson) {
        this.treeJson = treeJson;
    }

    @Override
    public String toString() {
        return "{ \n \"resultCode\":" + getResultCode() +
                "\n \"resultDescription\":" + getReturnDescription() +
                "\n \"errorType\":" + getErrorType() +
                "\n \"tree\":" +treeJson.toString() + "\n" +
                "}";
    }
}
