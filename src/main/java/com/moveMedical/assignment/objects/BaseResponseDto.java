package com.moveMedical.assignment.objects;

public class BaseResponseDto {

    private int resultCode;
    private String returnDescription;
    private String errorType;

    public BaseResponseDto(int resultCode, String errorType, String returnDescription) {
        this.resultCode = resultCode;
        this.errorType = errorType;
        this.returnDescription = returnDescription;
    }

    public BaseResponseDto() {
    }

    public int getResultCode() {
        return resultCode;
    }

    public void setResultCode(int resultCode) {
        this.resultCode = resultCode;
    }

    public String getReturnDescription() {
        return returnDescription;
    }

    public void setReturnDescription(String returnDescription) {
        this.returnDescription = returnDescription;
    }

    public String getErrorType() {
        return errorType;
    }

    public void setErrorType(String errorType) {
        this.errorType = errorType;
    }
}
