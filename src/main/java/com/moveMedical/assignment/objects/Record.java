package com.moveMedical.assignment.objects;

import java.util.List;

public class Record {

    private String name;

    private List<Record> children;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Record> getChildren() {
        return children;
    }

    public void setChildren(List<Record> children) {
        this.children = children;
    }
}
