package com.moveMedical.assignment.constants;

/**
 * Constants for setting DTOs
 */
public class NodeConstants {

    public static final int RESULT_SUCCESS = 0;
    public static final int ERROR_BASE_RESULT = 1000;
    public static final int ERROR_ON_INSERT_FAILURE = ERROR_BASE_RESULT + 100;
    public static final int ERROR_ON_INSERT_EXIST = ERROR_BASE_RESULT + 150;
    public static final int ERROR_ON_GET_TREE = ERROR_BASE_RESULT + 200;
    public static final int ERROR_GET_TREE_EMPTY = ERROR_BASE_RESULT + 300;
    public static final int ERROR_DELETE_NODE = ERROR_BASE_RESULT + 400;
    public static final int ERROR_DELETE_NODE_NO_NODE = ERROR_BASE_RESULT + 500;

    public static final String ERROR_TYPE_SUCCESS = "NO_ERROR";
    public static final String ERROR_TYPE_INSERT_FAILURE = "ERROR_TYPE_INSERT_RECORD";
    public static final String ERROR_TYPE_INSERT_NODE_EXIST = "ERROR_TYPE_INSERT_NODE_EXSIST";
    public static final String ERROR_TYPE_GET_TREE_FAILURE = "ERROR_TYPE_GET_TREE_FAILURE";
    public static final String ERROR_TYPE_GET_TREE_EMPTY = "ERROR_TYPE_GET_TREE_EMPTY";
    public static final String ERROR_TYPE_DELETE_FAILURE = "ERROR_TYPE_DELETE_FAILURE";
    public static final String ERROR_TYPE_DELETE_NO_NODE = "ERROR_TYPE_DELETE_NO_NODE";

    public static final String ERROR_DESCRIPTION_SUCESS = "Success";
    public static final String ERROR_DESCRIPTION_INTER_RECORD = "Error on inserting record into the database";
    public static final String ERROR_DESCRIPTION_INTER_DUPLICATES = " duplicate records being attemped to be inserted: ";
    public static final String ERROR_DESCRIPTION_GET_TREE_FAILURE = "Error on getting tree structure";
    public static final String ERROR_DESCRIPTION_GET_TREE_EMPTY = "No tree found from name searched";
    public static final String ERROR_DESCRIPTION_DELETE_FAILURE = "Error deleting node and it's children from database - name: ";
    public static final String ERROR_DESCRIPTION_DELETE_NO_NODE = "No node found when trying delete node with name: ";

}
