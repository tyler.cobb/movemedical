package com.moveMedical.assignment.dao;

import com.moveMedical.assignment.objects.Node;
import com.moveMedical.assignment.objects.NodeRowMapper;
import com.moveMedical.assignment.objects.Record;
import com.moveMedical.assignment.objects.RecordRowMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Dao that uses a namedParameterJdbcTemplate and sql
 * operations to hit the database and retrieve data.
 */
@Service
public class NodeDao {

    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;
    private static final Logger logger= LoggerFactory.getLogger(NodeDao.class);

    /**
     * SQL commands. NOTE** Normally would put this in a application.yml and pull in the values.
     */
    private final String insert = "INSERT into category (name) values (:name)";
    private final String insertChild = "INSERT into category (name, parent_id) values (:name, :parentId)";
    private final String delete = "DELETE FROM category WHERE name = :name";
    private final String exist = "SELECT id FROM category WHERE name = :name";
    private final String getChildren = "WITH RECURSIVE category_path (id, name, parent_id) AS\n" +
            "(\n" +
            "  SELECT id, name, parent_id \n" +
            "    FROM category\n" +
            "    WHERE parent_id = :parentId\n" +
            "  UNION ALL\n" +
            "  SELECT c.id, c.name, c.parent_id\n" +
            "    FROM category_path AS cp JOIN category AS c\n" +
            "      ON cp.id = c.parent_id\n" +
            ")\n" +
            "SELECT * FROM category_path\n" +
            "ORDER BY parent_id;";



    @Autowired
    public NodeDao (NamedParameterJdbcTemplate namedParameterJdbcTemplate){
        this.namedParameterJdbcTemplate = namedParameterJdbcTemplate;
    }

    /**
     * Insert method that hits the database with an insert command
     * @param name  record attempting to be inserted
     * @return list of strings that represents the duplicates
     */
    public List<String> insert(Record name) {
        logger.debug("Enter - insertDao");
        List<String> duplicates = new ArrayList<>();
        KeyHolder keyHolder = new GeneratedKeyHolder();
        MapSqlParameterSource parameters = new MapSqlParameterSource();
        parameters.addValue("name",name.getName());
        Integer id = checkIfRecordExist(name.getName());
        if(id == null) {
            logger.debug("Parent doesn't exist - attempting insert");
            namedParameterJdbcTemplate.update(insert, parameters, keyHolder, new String[]{"id"});
            id = keyHolder.getKey().intValue();
        }else {
            logger.debug("Parent does exist for record [" + name +"]");
            duplicates.add(name.getName());
        }
        if(!name.getChildren().stream().allMatch(l -> l == null)) {
            insertChildren(name.getChildren(), id, duplicates);
            logger.error("size" + duplicates.toString());
        }
        return duplicates;

    }

    /**
     *Insert children method that recursively inserts the children of the nodes
     *
     * @param records list of children nodes to be inserted
     * @param parentId parent Id of the root node
     * @param duplicates list of duplicates to add to if they are found
     * @return list of duplicates
     */
    protected List<String> insertChildren(List<Record> records, int parentId, List<String> duplicates) {
        for (Record record : records) {
            Integer recordExistCheck = checkIfRecordExist(record.getName());
            if (recordExistCheck == null) {
                MapSqlParameterSource paramMap = new MapSqlParameterSource();
                paramMap.addValue("name", record.getName());
                paramMap.addValue("parentId", parentId);
                KeyHolder keyHolder = new GeneratedKeyHolder();
                int result = namedParameterJdbcTemplate.update(insertChild, paramMap, keyHolder, new String[]{"id"});
                if (!record.getChildren().stream().allMatch(l -> l == null)) {
                    insertChildren(record.getChildren(), keyHolder.getKey().intValue(), duplicates);
                }
            }else{
                duplicates.add(record.getName());
                if (!record.getChildren().stream().allMatch(l -> l == null)) {
                    insertChildren(record.getChildren(), recordExistCheck, duplicates);
                }
            }
        }
        return duplicates;
    }

    /**
     * method that checks if a record exist in the database
     *
     * @param name name of the node to search for
     * @return integer value that represents if the node exist
     */
    protected Integer checkIfRecordExist(String name){
        Map<String, Object> paramMap = new HashMap<String, Object>();
        paramMap.put("name" , name);
        try {
            Integer id = namedParameterJdbcTemplate.queryForObject(exist, paramMap, Integer.class);
            logger.debug("Found Node with name" + name);
            return id;
        }catch(Exception ex){
            logger.debug("No record found before entry");
            return null;
        }
    }

    /**
     * method to get the list of nodes in a tree
     *
     * @param nodeName name of the parent node to search
     * @return list of nodes from the database
     */
    public List<Node> getTreeStructure(String nodeName) {
        logger.debug("Enter - getTreeStructureDao");
        Record record = new Record();
        List<Node> nodes = new ArrayList<>();
        Integer parent = checkIfRecordExist(nodeName);
        if (parent == null) {
            return null;
        } else {
            record.setName(nodeName);
            MapSqlParameterSource paramMap = new MapSqlParameterSource();
            paramMap.addValue("parentId", parent);
            Node parentNode = new Node(parent, nodeName, null);
            nodes.add(parentNode);
            nodes.addAll(namedParameterJdbcTemplate.query(getChildren, paramMap, new NodeRowMapper()));
        }
        return nodes;
    }

    /**
     * Method to delete the parent node and any children
     *
     * @param name name of the parent node to delete
     */
    public int deleteNodes(String name){
        logger.debug("Enter - deleteNodesDao");
        MapSqlParameterSource paramMap = new MapSqlParameterSource();
        paramMap.addValue("name", name);
        int deleteNumber = namedParameterJdbcTemplate.update(delete , paramMap);
        return deleteNumber;
    }
}
