package com.moveMedical.assignment.facade;

import com.moveMedical.assignment.constants.NodeConstants;
import com.moveMedical.assignment.dao.NodeDao;
import com.moveMedical.assignment.objects.BaseResponseDto;
import com.moveMedical.assignment.objects.Node;
import com.moveMedical.assignment.objects.Record;
import com.moveMedical.assignment.objects.TreeResponseDto;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.BeforeAll;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class NodeFacadeTest {

    NodeFacade nodeFacade;

    @Mock
    NodeDao mockNodeDao;

    @Before
    public void setUp(){
        nodeFacade = new NodeFacade(mockNodeDao);
    }

    @Test
    public void insertRecord_Success(){
        List<String> duplicates = new ArrayList<>();
        Record record = new Record();
        when(mockNodeDao.insert(any())).thenReturn(duplicates);
        BaseResponseDto baseResponseDto = nodeFacade.insertRecord(record);
        assertEquals(NodeConstants.RESULT_SUCCESS, baseResponseDto.getResultCode());
        assertEquals(NodeConstants.ERROR_TYPE_SUCCESS, baseResponseDto.getErrorType());
        assertEquals(NodeConstants.ERROR_DESCRIPTION_SUCESS, baseResponseDto.getReturnDescription());
    }

    @Test
    public void insertRecord_duplicates(){
        List<String> duplicates = new ArrayList<>();
        duplicates.add("name");
        Record record = new Record();
        when(mockNodeDao.insert(any())).thenReturn(duplicates);
        BaseResponseDto baseResponseDto = nodeFacade.insertRecord(record);
        assertEquals(NodeConstants.ERROR_ON_INSERT_EXIST, baseResponseDto.getResultCode());
        assertEquals(NodeConstants.ERROR_TYPE_INSERT_NODE_EXIST, baseResponseDto.getErrorType());
        assertEquals(duplicates.size() + NodeConstants.ERROR_DESCRIPTION_INTER_DUPLICATES + duplicates.toString(), baseResponseDto.getReturnDescription());
    }

    @Test
    public void insertRecord_failure(){
        Record record = new Record();
        when(mockNodeDao.insert(any())).thenThrow(ArrayIndexOutOfBoundsException.class);
        BaseResponseDto baseResponseDto = nodeFacade.insertRecord(record);
        assertEquals(NodeConstants.ERROR_ON_INSERT_FAILURE, baseResponseDto.getResultCode());
        assertEquals(NodeConstants.ERROR_TYPE_INSERT_FAILURE, baseResponseDto.getErrorType());
        assertEquals(NodeConstants.ERROR_DESCRIPTION_INTER_RECORD, baseResponseDto.getReturnDescription());
    }

    @Test
    public void getTreeStructure_Success(){
        when(mockNodeDao.getTreeStructure(any())).thenReturn(returnNodeListList());
        TreeResponseDto treeResponseDto = nodeFacade.getTreeStructure("name");
        assertNotNull(treeResponseDto.getTreeJson());
        assertEquals(NodeConstants.RESULT_SUCCESS, treeResponseDto.getResultCode());
        assertEquals(NodeConstants.ERROR_TYPE_SUCCESS, treeResponseDto.getErrorType());
        assertEquals(NodeConstants.ERROR_DESCRIPTION_SUCESS, treeResponseDto.getReturnDescription());
    }

    @Test
    public void getTreeStructure_NullTree(){
        when(mockNodeDao.getTreeStructure(any())).thenReturn(null);
        TreeResponseDto treeResponseDto = nodeFacade.getTreeStructure("name");
        assertEquals("" , treeResponseDto.getTreeJson());
        assertEquals(NodeConstants.ERROR_GET_TREE_EMPTY, treeResponseDto.getResultCode());
        assertEquals(NodeConstants.ERROR_TYPE_GET_TREE_EMPTY, treeResponseDto.getErrorType());
        assertEquals(NodeConstants.ERROR_DESCRIPTION_GET_TREE_EMPTY, treeResponseDto.getReturnDescription());
    }

    @Test
    public void getTreeStructure_exception(){
        when(mockNodeDao.getTreeStructure(any())).thenThrow(ArrayIndexOutOfBoundsException.class);
        TreeResponseDto treeResponseDto = nodeFacade.getTreeStructure("name");
        assertEquals("" , treeResponseDto.getTreeJson());
        assertEquals(NodeConstants.ERROR_ON_GET_TREE, treeResponseDto.getResultCode());
        assertEquals(NodeConstants.ERROR_TYPE_GET_TREE_FAILURE, treeResponseDto.getErrorType());
        assertEquals(NodeConstants.ERROR_DESCRIPTION_GET_TREE_FAILURE, treeResponseDto.getReturnDescription());
    }

    @Test
    public void deleteTree_Success(){
        when(mockNodeDao.deleteNodes(any())).thenReturn(1);
        BaseResponseDto baseResponseDto = nodeFacade.deleteTree("name");
        assertEquals(NodeConstants.RESULT_SUCCESS, baseResponseDto.getResultCode());
        assertEquals(NodeConstants.ERROR_TYPE_SUCCESS, baseResponseDto.getErrorType());
        assertEquals(NodeConstants.ERROR_DESCRIPTION_SUCESS, baseResponseDto.getReturnDescription());
    }

    @Test
    public void deleteTree_NoneFound(){
        when(mockNodeDao.deleteNodes(any())).thenReturn(0);
        String nodeSearched = "name";
        BaseResponseDto baseResponseDto = nodeFacade.deleteTree(nodeSearched);
        assertEquals(NodeConstants.ERROR_DELETE_NODE_NO_NODE, baseResponseDto.getResultCode());
        assertEquals(NodeConstants.ERROR_TYPE_DELETE_NO_NODE, baseResponseDto.getErrorType());
        assertEquals(NodeConstants.ERROR_DESCRIPTION_DELETE_NO_NODE + nodeSearched, baseResponseDto.getReturnDescription());
    }

    @Test
    public void deleteTree_Exception(){
        when(mockNodeDao.deleteNodes(any())).thenThrow(NullPointerException.class);
        String nodeSearched = "name";
        BaseResponseDto baseResponseDto = nodeFacade.deleteTree(nodeSearched);
        assertEquals(NodeConstants.ERROR_DELETE_NODE, baseResponseDto.getResultCode());
        assertEquals(NodeConstants.ERROR_TYPE_DELETE_FAILURE, baseResponseDto.getErrorType());
        assertEquals(NodeConstants.ERROR_DESCRIPTION_DELETE_FAILURE + nodeSearched, baseResponseDto.getReturnDescription());
    }

    private List<Node> returnNodeListList(){
        List<Node> nodes = new ArrayList<>();
        Node node = new Node(1, "node1", null);
        Node node1 = new Node(2, "node2", 1);
        Node node2 = new Node(3, "node3", 1);
        Node node3 = new Node(4, "node4", 3);
        nodes.add(node);
        nodes.add(node1);
        nodes.add(node2);
        nodes.add(node3);
        return nodes;
    }
}
