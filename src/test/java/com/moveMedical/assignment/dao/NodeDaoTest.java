package com.moveMedical.assignment.dao;

import com.moveMedical.assignment.objects.Node;
import com.moveMedical.assignment.objects.NodeRowMapper;
import com.moveMedical.assignment.objects.Record;
import io.swagger.models.auth.In;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.KeyHolder;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class NodeDaoTest {

    NodeDao nodeDao;

    @Mock
    NamedParameterJdbcTemplate mockNamedParameterJdbcTemplate;

    @Mock
    KeyHolder mockKeyHolder;

    @Before
    public void setup(){
        this.nodeDao = new NodeDao(mockNamedParameterJdbcTemplate);
    }

    @Test
    public void insert_success_RootNode(){
        List<String> insertString = new ArrayList<>();
        MapSqlParameterSource parameters = new MapSqlParameterSource();
        Record record = new Record();
        List<Record> records = new ArrayList<>();
        record.setChildren(records);
        Map<String, Object> stringObjectMap = new HashMap<>();
        when(mockNamedParameterJdbcTemplate.queryForObject(any(String.class), anyMap(), eq(Integer.class))).thenReturn(5);
        insertString = nodeDao.insert(record);
        verify(mockNamedParameterJdbcTemplate,times(1)).queryForObject(any(String.class), anyMap(), eq(Integer.class));
        assertEquals(1, insertString.size());
    }

    @Test
    public void checkRecord_success(){
        when(mockNamedParameterJdbcTemplate.queryForObject(any(String.class), anyMap(), eq(Integer.class))).thenReturn(1);
        Integer duplicates = nodeDao.checkIfRecordExist("name");
        assertEquals(1, duplicates.intValue());
    }

    @Test
    public void checkRecordExcept_Exception(){
        when(mockNamedParameterJdbcTemplate.queryForObject(any(String.class), anyMap(), eq(Integer.class))).thenThrow(NumberFormatException.class);
        Integer duplicates = nodeDao.checkIfRecordExist("name");
        assertEquals(null, duplicates);
    }

    @Test
    public void getTreeStructure_success(){
        when(mockNamedParameterJdbcTemplate.queryForObject(any(String.class), anyMap(), eq(Integer.class))).thenReturn(1);
        when(mockNamedParameterJdbcTemplate.query(anyString(), any(MapSqlParameterSource.class), any(NodeRowMapper.class))).thenReturn(returnNodeListList());
        List<Node> nodes = new ArrayList<>();
        nodes = nodeDao.getTreeStructure("name");
        assertEquals(5 ,nodes.size());
    }

    @Test
    public void getTreeStructure_parentNull(){
        List<Node> nodes = new ArrayList<>();
        nodes = nodeDao.getTreeStructure("name");
        assertEquals(null ,nodes);
    }

    @Test
    public void deleteNodes_success(){
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        when(mockNamedParameterJdbcTemplate.update(anyString(), any(MapSqlParameterSource.class))).thenReturn(1);
        int result = nodeDao.deleteNodes("name");
        assertEquals(1, result);
    }

    private List<Node> returnNodeListList(){
        List<Node> nodes = new ArrayList<>();
        Node node = new Node(1, "node1", null);
        Node node1 = new Node(2, "node2", 1);
        Node node2 = new Node(3, "node3", 1);
        Node node3 = new Node(4, "node4", 3);
        nodes.add(node);
        nodes.add(node1);
        nodes.add(node2);
        nodes.add(node3);
        return nodes;
    }




}
