# MoveMedical Assignment
Create RESTful web services to (1) create, (2) read/list, and (3) delete records within a hierarchy/tree.

## Project Dependencies
- Spring Boot
- H2 database
- Maven 
- JUnit  (68% line coverage. All logic is unit tested. Didn't test getters and setters)

## Running the app

```bash
mvn spring-boot:run
```
- Navigate to http://localhost:8080/swagger-ui.html to see operations

## Usage
Three different operations

- InsertRecord : Inserts a record using the schema provided (node and node's children and so on). If a node already exist in the database then the response will notify the user but continue to attempt to enter the other nodes.
- getTree : Searching by name, the user will get a tree in return showing the parents and children in a hierarchy structure. If no node is found the user will be notified.
- deleteTree : Based on the name given, this will attempt to delete the node and it's children based on the name given. If no node is found the user will be notified.


## Git Location
https://gitlab.com/tyler.cobb/movemedical

## Author
[Tyler Cobb](https://www.linkedin.com/in/tyler-s-cobb)